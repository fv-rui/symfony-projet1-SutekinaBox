<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Box;
use AppBundle\Entity\Notification;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request,  \Swift_Mailer $mailer)
    {
        $em = $this->getDoctrine()->getManager();

        $boxes = $em->getRepository('AppBundle:Box')->findByState("finish");
//        $boxes = $em->getRepository('AppBundle:Box')->findAll();

        $notification = $em->getRepository('AppBundle:Notification')->findByEtat("Non vu");
        $nbNotif = count($notification);


        $to      = 'mirmo91@hotmail.com';
        $subject = 'le sujet';
        $body = 'Bonjour !';

      $mail = (new \Swift_Message($subject))
            ->setFrom('admin@sutekinabox.com')
            ->setTo($to)
            ->setBody($body);

        $mailer->send($mail);



        return $this->render('default/index.html.twig', array(
            'boxes' => $boxes,
            'nbNotif' => $nbNotif,
        ));
    }

    /**
     * @Route("/show/{id}", name="showBoxUser")
     * @Method("GET")
     */
    public function showBoxUserAction(Request $request, Box $box)
    {
        return $this->render('default/showBoxUser.html.twig', array(
            'box' => $box,
        ));
    }

    /**
     * @Route("admin/notifictions", name="showNotifiction")
     */
    public function showNotifictionAction()
    {
        $em = $this->getDoctrine()->getManager();
        $notification = $em->getRepository('AppBundle:Notification')->findBy(array(), array('etat' => 'ASC'));

        return $this->render('admin/showNotifiction.html.twig', array(
            'notifications' => $notification,
        ));
    }

    /**
     * @Route("admin/{id}/notifictionsVu", name="notifictionVu")
     * @Method("GET")
     */
    public function notifictionVuAction(Notification $notification)
    {
        $em = $this->getDoctrine()->getManager();
        $notification->setEtat("Vu");
        $em->flush();
        return $this->redirectToRoute('box_index');
    }

}
