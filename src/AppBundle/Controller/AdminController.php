<?php
/**
 * Created by PhpStorm.
 * User: hello
 * Date: 31/10/2017
 * Time: 13:26
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Box;
use AppBundle\Entity\Notification;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Yaml\Yaml;


class AdminController extends Controller
{
    /**
     * @Route("/admin/box/{id}/ajouter", name="ajoutProduitBox")
     * @Method({"GET", "POST"})
     */
    public function ajoutProduitBoxAction(Request $request, Box $box)
    {
        $em = $this->getDoctrine()->getManager();

        $budgetRestant = $box->getBudgetRestant();
        $produits = $em->getRepository('AppBundle:Produit')->findBudgetRestant($budgetRestant);

        $collectionlisteProduit = $box->getListeProduits();
        foreach ($produits as $key => $produit){
            if ($collectionlisteProduit->contains($produit)) {
                unset($produits[$key]);
            }
        }

        return $this->render('admin/box/ajoutProduitBox.html.twig', [
            'box' => $box,
            'produits' => $produits
        ]);
    }

    /**
     * @Route("/admin/box/{id}/{idaj}/ajout", name="produitBoxAjouter")
     * @Method("GET")
     */
    public function produitBoxAjouterAction(Box $box, $idaj)
    {
        $em = $this->getDoctrine()->getManager();

        $produit = $em->getRepository('AppBundle:Produit')->find($idaj);

        $session = new Session();

        $collectionlisteProduit = $box->getListeProduits();
        if(!$collectionlisteProduit->contains($produit)){

            $budgetRestant = $box->getBudgetRestant()-$produit->getPrix();
            $box->setBudgetRestant($budgetRestant);

            $box->addListeProduit($produit);

            $produitsrestant = $em->getRepository('AppBundle:Produit')->findBudgetRestant($budgetRestant);

            foreach ($produitsrestant as $key => $produit){
                if ($collectionlisteProduit->contains($produit)) {
                    unset($produitsrestant[$key]);
                }
            }

            $message = 'Le poduit "'.$produit->getNom().'" a bien été a jouté';
            $session->getFlashBag()->add('success', $message);
        } else {
            $message = 'Le poduit "'.$produit->getNom().'" est déjà dans la box !';
            $session->getFlashBag()->add('danger', $message);
        }

        $em->flush();

        return $this->redirectToRoute('ajoutProduitBox', array('id'=>$box->getId()));
    }

    /**
     * @Route("/admin/box/{id}/{idsup}/remove", name="removeProduitBox")
     * @Method("GET")
     */
    public function suprimerProduitBoxAction(Box $box, $idsup)
    {
        $em = $this->getDoctrine()->getManager();

        $produit = $em->getRepository('AppBundle:Produit')->find($idsup);

        $session = new Session();

        $collectionlisteProduit = $box->getListeProduits();

        $budgetRestant = $box->getBudgetRestant()+$produit->getPrix();
        $box->setBudgetRestant($budgetRestant);

        $box->removeListeProduit($produit);

        if($collectionlisteProduit->isEmpty()){
            $box->setState("empty");
        }else{
            $box->setState("fill");
        }

        $message = "Le poduit ".$produit->getNom().' a bien été retiré de la box.';
        $session->getFlashBag()->add('success', $message);

        $em->flush();

        return $this->redirectToRoute('ajoutProduitBox', array('id'=>$box->getId()));
    }

    /**
     * @Route("/admin/box/{id}/completed", name="completedBox", requirements={"id": "\d+"})
     * @Method("GET")
     */
    public function completedBoxAction(Box $box)
    {
        $em = $this->getDoctrine()->getManager();

        $budgetRestant = $box->getBudgetRestant();

        $produitsrestant = $em->getRepository('AppBundle:Produit')->findBudgetRestant($budgetRestant);
        $collectionlisteProduit = $box->getListeProduits();

        foreach ($produitsrestant as $key => $produit){
            if ($collectionlisteProduit->contains($produit)) {
                unset($produitsrestant[$key]);
            }
        }

        if(empty($produitsrestant)){
            $box->setState("completed");

            $notification = new Notification();

            $notification->setRole("ROLE_ACHAT");
            $notification->setType("box completed");
            $notification->setDescription('La Box "'.$box->getBoxName().'" est complete. En attente de verification.');

            $em->persist($notification);
        }
        $em->flush();

        return $this->redirectToRoute('box_index');
    }
}