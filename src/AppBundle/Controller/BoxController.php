<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Box;
use AppBundle\Entity\Notification;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Box controller.
 *
 * @Route("admin/box")
 */
class BoxController extends Controller
{
    /**
     * Lists all box entities.
     *
     * @Route("/", name="box_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $boxes = $em->getRepository('AppBundle:Box')->findAll();
        return $this->render('admin/box/index.html.twig', array(
            'boxes' => $boxes,
        ));
    }

    /**
     * Creates a new box entity.
     *
     * @Route("/new", name="box_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $box = new Box();
        $form = $this->createForm('AppBundle\Form\BoxType', $box);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $box->setBudgetRestant($box->getBudgetMax());
//            $box->setState("vide");

            $em->persist($box);
            $em->flush();

            return $this->redirectToRoute('box_show', array('id' => $box->getId()));
        }

        return $this->render('admin/box/new.html.twig', array(
            'box' => $box,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a box entity.
     *
     * @Route("/{id}", name="box_show")
     * @Method("GET")
     */
    public function showAction(Box $box)
    {
        return $this->render('admin/box/show.html.twig', array(
            'box' => $box,
        ));
    }
    /**
     * @Route("/{id}/{idp}/{action}/", name="confAndNonConf")
     * @Method("GET")
     */
    public function confAndNonConfAction(Box $box, $idp, $action)
    {
        $em = $this->getDoctrine()->getManager();

        $collectionlisteProduits = $box->getListeProduits();

        $produitcol = $collectionlisteProduits->get($idp);

        $produitcol->setStatut($action);

        $box->getListeProduits()->set($idp,$produitcol);

        $arrayCollectionProduit = $collectionlisteProduits->toArray();

        $conformeMax = count($arrayCollectionProduit);
        $conforme = 0;

        foreach ($arrayCollectionProduit as $produitValue){
            if ($produitValue->getStatut() == "Conforme"){
                $conforme++;
            }
        }

        if ($conforme === $conformeMax){
            $box->setState("verified");

            $notification = new Notification();

            $notification->setRole("ROLE_MARKETING");
            $notification->setType("box verified");
            $notification->setDescription('La Box "'.$box->getBoxName().'" est conforme.');

            $em->persist($notification);

            $em->flush();

            return $this->redirectToRoute('box_index');
        } else {
            $box->setState("completed");
            $em->flush();
            return $this->redirectToRoute('box_show', array('id'=>$box->getId()));
        }
    }

    /**
     * @Route("/{id}/finishBox", name="finishBox")
     * @Method({"GET", "POST"})
     */
    public function finishBoxAction(Box $box)
    {
        $em = $this->getDoctrine()->getManager();
        $box->setState("finish");
        $em->flush();

        return $this->redirectToRoute('box_index');
    }

    /**
     * Displays a form to edit an existing box entity.
     *
     * @Route("/{id}/edit", name="box_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Box $box)
    {
        $editForm = $this->createForm('AppBundle\Form\BoxType', $box);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('box_edit', array('id' => $box->getId()));
        }

        return $this->render('admin/box/edit.html.twig', array(
            'box' => $box,
            'edit_form' => $editForm->createView(),
        ));
    }


    /**
     * Deletes a box entity.
     *
     * @Route("/{id}/box_delete", name="box_delete")
     * @Method("GET")
     */
    public function deleteAction(Box $box)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($box);
        $em->flush();

        return $this->redirectToRoute('box_index');
    }

}
