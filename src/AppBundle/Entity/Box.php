<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Box
 *
 * @ORM\Table(name="box")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BoxRepository")
 */
class Box
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="boxName", type="string", length=255)
     */
    private $boxName;

    /**
     * @var float
     *
     * @ORM\Column(name="budgetMax", type="float")
     */
    private $budgetMax;

    /**
     * @var float
     *
     * @ORM\Column(name="budgetRestant", type="float")
     */
    private $budgetRestant;

    /**
     * @var string
     *
     * @ORM\Column(name="themeBox", type="string", length=255)
     */
    private $themeBox;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=255)
     */
    private $state;

    /**
     * @var Produit
     *
     * @ORM\ManyToMany(targetEntity="Produit")
     */
    private $listeProduits;



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->listeProduits = new \Doctrine\Common\Collections\ArrayCollection();
        $this->state = "empty";
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set budgetMax
     *
     * @param integer $budgetMax
     *
     * @return Box
     */
    public function setBudgetMax($budgetMax)
    {
        $this->budgetMax = $budgetMax;

        return $this;
    }

    /**
     * Get budgetMax
     *
     * @return int
     */
    public function getBudgetMax()
    {
        return $this->budgetMax;
    }

    /**
     * Set themeBox
     *
     * @param string $themeBox
     *
     * @return Box
     */
    public function setThemeBox($themeBox)
    {
        $this->themeBox = $themeBox;

        return $this;
    }

    /**
     * Get themeBox
     *
     * @return string
     */
    public function getThemeBox()
    {
        return $this->themeBox;
    }

    /**
     * Set boxName
     *
     * @param string $boxName
     *
     * @return Box
     */
    public function setBoxName($boxName)
    {
        $this->boxName = $boxName;

        return $this;
    }

    /**
     * Get boxName
     *
     * @return string
     */
    public function getBoxName()
    {
        return $this->boxName;
    }

    /**
     * Set budgetRestant
     *
     * @param float $budgetRestant
     *
     * @return Box
     */
    public function setBudgetRestant($budgetRestant)
    {
        $this->budgetRestant = $budgetRestant;

        return $this;
    }

    /**
     * Get budgetRestant
     *
     * @return float
     */
    public function getBudgetRestant()
    {
        return $this->budgetRestant;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return Box
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Add listeProduit
     *
     * @param \AppBundle\Entity\Produit $listeProduit
     *
     * @return Box
     */
    public function addListeProduit(\AppBundle\Entity\Produit $listeProduit)
    {
        $this->listeProduits[] = $listeProduit;

        return $this;
    }

    /**
     * Remove listeProduit
     *
     * @param \AppBundle\Entity\Produit $listeProduit
     */
    public function removeListeProduit(\AppBundle\Entity\Produit $listeProduit)
    {
        $this->listeProduits->removeElement($listeProduit);
    }

    /**
     * Get listeProduits
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getListeProduits()
    {
        return $this->listeProduits;
    }

    /**
     * Set listeProduits
     *
     * @param \Doctrine\Common\Collections\ArrayCollection $collectionProduits
     */
    public function setListeProduits(\Doctrine\Common\Collections\ArrayCollection $collectionProduits)
    {
        $this->listeProduits = $collectionProduits;
    }
}
