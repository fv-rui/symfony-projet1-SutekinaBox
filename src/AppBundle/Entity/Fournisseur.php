<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Fournisseur
 *
 * @ORM\Table(name="fournisseur")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FournisseurRepository")
 */
class Fournisseur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="siret", type="string", length=255)
     */
    private $siret;

    /**
     * @var array
     *
     * @ORM\OneToMany(targetEntity="Produit", mappedBy="fournisseur", cascade={"persist"})
     */
    private $listeProduits;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->listeProduits = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Fournisseur
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set siret
     *
     * @param string $siret
     *
     * @return Fournisseur
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * Get siret
     *
     * @return string
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * Add Produit
     *
     * @param Produit $produit
     *
     * @return Fournisseur
     */
    public function addListeProduits(Produit $produit)
    {
        $this->listeProduits[] = $produit;

        return $this;
    }

    /**
     * Remove Produit
     *
     * @param Produit $produit
     */
    public function removeBadgeUnlock(Produit $produit)
    {
        $this->listeProduits->removeElement($produit);
    }

    /**
     * Get listeProduits
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getListeProduits()
    {
        return $this->listeProduits;
    }

    /**
     * @return array
     */
    public function getListeProduitsDisponible()
    {
        return $this->listeProduitsDisponible;
    }

    /**
     * @param array $listeProduitsDisponible
     */
    public function setListeProduitsDisponible($listeProduitsDisponible)
    {
        $this->listeProduitsDisponible = $listeProduitsDisponible;
    }
}
