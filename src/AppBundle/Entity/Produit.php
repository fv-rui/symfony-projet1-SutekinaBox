<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Produits
 *
 * @ORM\Table(name="produit")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProduitsRepository")
 */
class Produit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255)
     */
    private $reference;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float")
     */
    private $prix;

    /**
     * @var array
     *
     * @ORM\Column(type="json_array")
     */
    private $statut;

    /**
     * @var Box
     *
     * @ORM\ManyToMany(targetEntity="Box")
     */
    private $listeBox;

    /**
     * @var Fournisseur
     *
     * @ORM\ManyToOne(targetEntity="Fournisseur", inversedBy="listeProduits", cascade={"persist"})
     */
    private $fournisseur;



    public function __construct()
    {
        $this->setStatut([0 => 'Disponible']);
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return Produit
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Produit
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prix
     *
     * @param float $prix
     *
     * @return Produit
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set statut
     *
     * @param array $statut
     *
     * @return Produit
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return array
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set listeBox
     *
     * @param string $listeBox
     *
     * @return Produit
     */
    public function setListeBox($listeBox)
    {
        $this->listeBox = $listeBox;

        return $this;
    }

    /**
     * Get listeBox
     *
     * @return string
     */
    public function getListeBox()
    {
        return $this->listeBox;
    }

    /**
     * Set fournisseur
     *
     * @param string $fournisseur
     *
     * @return Produit
     */
    public function setFournisseur($fournisseur)
    {
        $this->fournisseur = $fournisseur;

        return $this;
    }

    /**
     * Get fournisseur
     *
     * @return string
     */
    public function getFournisseur()
    {
        return $this->fournisseur;
    }
}
