<?php
/**
 * Created by PhpStorm.
 * User: hello
 * Date: 03/11/2017
 * Time: 11:24
 */

namespace AppBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SelectProductType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('listeProduits', EntityType::class,
                [
                    'class' => 'AppBundle\Entity\Produit',
                    'choice_label' => 'nom',
                    'multiple' => true,
                    'expanded' => false
                ])

        ;
//
//        $builder->add('produit', HiddenType::class, array(
//            'data' => 'abcdef',
//        ));
    }

//    public function configureOptions(OptionsResolver $resolver)
//    {
//        $resolver->setDefaults(array(
//            'data_class' => 'AppBundle\Entity\Produit'
//        ));
//    }
}