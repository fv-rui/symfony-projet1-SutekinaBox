<?php

/*
 * This file is part of the td23 package.
 *
 * (c) Matthieu Mota <matthieu@boxydev.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Fournisseur;
use AppBundle\Entity\Produit;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Yaml\Yaml;

class LoadDataFixtures extends Fixture
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $infos = Yaml::parse(file_get_contents(".\app\config\configData\listeFournisseurs.yml"));


        $fournissuer_1 = new Fournisseur();
        $fournissuer_1->setNom($infos['Fournisseur_1']['name']);
        $fournissuer_1->setSiret($infos['Fournisseur_1']['siret']);
        $fournissuer_1->setListeProduitsDisponible($infos['Fournisseur_1']['produits']);

        $fournissuer_2 = new Fournisseur();
        $fournissuer_2->setNom($infos['Fournisseur_2']['name']);
        $fournissuer_2->setSiret($infos['Fournisseur_2']['siret']);
        $fournissuer_2->setListeProduitsDisponible($infos['Fournisseur_2']['produits']);


        // instances des produits du Fournisseur 1
        $produit_1_F1 = new Produit();
        $produit_1_F1->setReference($infos['Fournisseur_1']['produits'][0]['ref']);
        $produit_1_F1->setNom($infos['Fournisseur_1']['produits'][0]['nom']);
        $produit_1_F1->setPrix($infos['Fournisseur_1']['produits'][0]['prix']);
        $produit_1_F1->setFournisseur($fournissuer_1);

        $produit_2_F1 = new Produit();
        $produit_2_F1->setReference($infos['Fournisseur_1']['produits'][1]['ref']);
        $produit_2_F1->setNom($infos['Fournisseur_1']['produits'][1]['nom']);
        $produit_2_F1->setPrix($infos['Fournisseur_1']['produits'][1]['prix']);
        $produit_2_F1->setFournisseur($fournissuer_1);

        // ajout les instances des produits du Fournisseur 1
        $fournissuer_1->addListeProduits($produit_1_F1);
        $fournissuer_1->addListeProduits($produit_2_F1);


        // instances des produits du Fournisseur 2
        $produit_1_F2 = new Produit();
        $produit_1_F2->setReference($infos['Fournisseur_2']['produits'][0]['ref']);
        $produit_1_F2->setNom($infos['Fournisseur_2']['produits'][0]['nom']);
        $produit_1_F2->setPrix($infos['Fournisseur_2']['produits'][0]['prix']);
        $produit_1_F2->setFournisseur($fournissuer_2);

        $produit_2_F2 = new Produit();
        $produit_2_F2->setReference($infos['Fournisseur_2']['produits'][1]['ref']);
        $produit_2_F2->setNom($infos['Fournisseur_2']['produits'][1]['nom']);
        $produit_2_F2->setPrix($infos['Fournisseur_2']['produits'][1]['prix']);
        $produit_2_F2->setFournisseur($fournissuer_2);

        $produit_3_F2 = new Produit();
        $produit_3_F2->setReference($infos['Fournisseur_2']['produits'][2]['ref']);
        $produit_3_F2->setNom($infos['Fournisseur_2']['produits'][2]['nom']);
        $produit_3_F2->setPrix($infos['Fournisseur_2']['produits'][2]['prix']);
        $produit_3_F2->setFournisseur($fournissuer_2);

        // ajout les instances des produits du Fournisseur 2
        $fournissuer_2->addListeProduits($produit_1_F2);
        $fournissuer_2->addListeProduits($produit_2_F2);
        $fournissuer_2->addListeProduits($produit_3_F2);

        // perssister les fournisseurs
        $manager->persist($fournissuer_1);
        $manager->persist($fournissuer_2);

        // persister les produits
        $manager->persist($produit_1_F1);
        $manager->persist($produit_2_F1);
        $manager->persist($produit_1_F2);
        $manager->persist($produit_2_F2);

        $manager->flush();
    }
}